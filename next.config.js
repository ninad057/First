/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['i.ibb.co','i.postimg.cc','lh3.googleusercontent.com','media.licdn.com','icedrive.net','drive.google.com'],
  },
}

module.exports = nextConfig
